﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tut04_ex03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a whole Number");
            var input = Console.ReadLine();
            var a = 0;
            bool value = int.TryParse(input, out a);

            if(value == true)
            {
                Console.WriteLine($"You entered the number {input}");
            }
            if(value == false)
            {
                Console.WriteLine("You did not enter a number");
            }
        }
    }
}
